
# get the input
file = open('day7-input.txt', 'r').readline()
crabs = list(map(int, file.split(',')))

# part 1

positions = set(crabs)
fuel_use_p1 = list()

# loop through the positions and crabs and determine how much
# fuel it would take for each crab to reach each position

for position in positions:
    fuel = 0
    for crab in crabs:
        fuel += abs(position - crab)
    fuel_use_p1.append(fuel)

# output

print('Part 1: ', min(fuel_use_p1))

# part 2

fuel_use_p2 = list()

for position in positions:
    fuel = 0
    for crab in crabs:
        difference = abs(position - crab)
        for val in range(1, difference):
            fuel += val
    fuel_use_p2.append(fuel)

    
# output

print('Part 2: ', min(fuel_use_p2))
