# part 1

file = open('day1-input.txt', 'r')
values = file.readlines()

count = 0
for i in range(1, len(values)):
    if int(values[i]) > int(values[i-1]):
        count += 1

print('Part 1: ', count)

# part 2

count = 0
for i in range(3, len(values)):
    if int(values[i]) > int(values[i-3]):
        count += 1

print('Part 2: ', count)



