
# get the input
file = open('day6-input.txt', 'r').readline()
fish = list(map(int, file.split(',')))

for day in range(0, 256):
    for i in range(0, len(fish)):
        lastFish = len(fish) - 1
        if i <= lastFish:
            if fish[i] == 0:
                fish[i] = 6
                fish.append(8)
            else:
                fish[i] -= 1
    print("Day ", day, ": ", len(fish))

print(len(fish))
