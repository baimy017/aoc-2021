depth = 0
horizontal = 0
aim = 0

file = open('day2-submarine-commands.txt', 'r')
commands = file.readlines()
file.close()


for command in commands:
    action = command.split()
    if(action[0] == 'forward'):
        horizontal += int(action[1])
        depth += aim * int(action[1])
    elif(action[0] == 'down'):
        aim += int(action[1])
    elif(action[0] == 'up'):
        aim -= int(action[1])

result = depth * horizontal
print('Depth: ', depth, '\nHorizontal Position: ', horizontal, '\nAnswer: ', result)



''' 
for command in commands:
    action = command.split()
    if(action[0] == 'forward'):
        horizontal += int(action[1])
    elif(action[0] == 'down'):
        depth += int(action[1])
    elif(action[0] == 'up'):
        depth -= int(action[1])

result = depth * horizontal
print('Depth: ', depth, '\nHorizontal Position: ', horizontal, '\nAnswer: ', result)
'''
