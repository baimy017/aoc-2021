
# get the input
file = open('day3-input.txt', 'r')
binary = file.readlines()

# part one

# set up the counting arrays
length = len(binary[0])
zeroes = []
ones = []

for i in range(0, length-1):
    zeroes.append(0)
    ones.append(0)

# begin the counting
for bits in binary:
    for i in range(0, len(bits)-1):
        if bits[i] == '0':
            zeroes[i] += 1
        elif bits[i] == '1':
            ones[i] += 1

# set up gamma rate and epsilon
gamma = ''
epsilon = ''

# determine which are more common
for i in range(0, length-1):
    if zeroes[i] > ones[i]:
        gamma += '0'
        epsilon += '1'
    else:
        gamma += '1'
        epsilon += '0'

print('Part 1 Answer: ', int(gamma, 2) * int(epsilon, 2))

# part two

def findMoreCommon(values, index):
    if len(values) == 1:
        return values
    zeroes = []
    ones = []
    for val in values:
        if val[index] == '0':
            zeroes.append(val)
        else:
            ones.append(val)
    if len(zeroes) > len(ones):
        result = findMoreCommon(zeroes, index + 1)
    else:
        result = findMoreCommon(ones, index + 1)
    return result

def findLeastCommon(values, index):
    if len(values) == 1:
        return values
    zeroes = []
    ones = []
    for val in values:
        if val[index] == '0':
            zeroes.append(val)
        else:
            ones.append(val)
    if len(zeroes) <= len(ones):
        result = findLeastCommon(zeroes, index + 1)
    else:
        result = findLeastCommon(ones, index + 1)
    return result

oxygen = findMoreCommon(binary, 0)[0]
scrubber = findLeastCommon(binary, 0)[0]

print('Oxygen: ', int(oxygen, 2), ' Binary: ', oxygen)
print('Scubber: ', int(scrubber, 2), 'Binary: ', scrubber)
print('Answer: ', int(oxygen, 2) * int(scrubber, 2))

        
