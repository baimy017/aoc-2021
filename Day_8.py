ACCEPTED_LENGTHS = [2, 3, 4, 7]

# part 1 function

def getCount(value):
    sliceIndex = value.find('|') + 1
    slicedValue = value[sliceIndex:]
    values = slicedValue.split()

    count = 0
    for val in values:
        if len(val) in ACCEPTED_LENGTHS:
            count += 1

    return count

# get the input
file = open('day8-input.txt', 'r').readlines()
values = list()

# part 1

for line in file:
    values.append(getCount(line))

print(sum(values))
